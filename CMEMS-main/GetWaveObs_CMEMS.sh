#!/bin/bash 
# Downloads the wave observations from CMEMS  
# See the README.md ==> .wgetrc

date_start=$(date -d "1 day ago" '+%Y%m%d')
date_end=$(date '+%Y%m%d')

output_path=./data
altimeters=("al" "c2" "cfo" "h2b" "j3" "s3a" "s3b")
sar=('s1a' 's1b')

cdate=$date_start
while [[ $cdate -le $date_end ]]; do
    echo Downloading data for $cdate
    
    #1. In-situ integrated parameters from moorings and drifters
    ./spc_insitu.sh $cdate $output_path 

    #2. In-situ wave spectra
    ./wav_insitu.sh $cdate $output_path 
    
    #3. SWH from Altimeters
    for sat in "${altimeters[@]}"; do
        ./swh_sat.sh "${sat}" $cdate $output_path 
    done

    #4. SAR wave spectra
    for sat in "${sar[@]}"; do
        ./spc_sat.sh "${sat}" $cdate $output_path 
    done

cdate=$(date -d "$cdate + 1 day" +%Y%m%d)
done

# CMEMS
Scrips to download data from the CMEMS

Short description of the main scripts:

| Script       | Description                                       |
|--------------|:-------------------------------------------------:|  
|spc_insitu.sh | Downloads insitu wave spectra                     |
|wav_insitu.sh | Downloads insitu integrated wave parameters       |
|swh_sat.sh    | Downloads significant wave height from altimeters |
|spc_sat.sh    | Downloads SAR wave specra                         |

The GetWaveObs_CMEMS.sh is a driver, it can be used as an example

To download from CMEMS, the user has to create:
1. an account at the CMEMS portal
2. the ~/.wgetrc 

The ~/.wgetrc should have atleast the following two entries:

`ftp_user=<Username>`
`ftp_password=<password>`
 

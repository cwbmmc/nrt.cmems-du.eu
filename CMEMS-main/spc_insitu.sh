#!/bin/bash
# download insitu wave observations hosted at CMEMS ftp server

set -eu

if [[ $# != 2 ]]; then
    echo 'To use the script: $0 sat date local_path
    date	: Date in the following format YYYYMMDD 
    local_path	: local/path/to/save/the/data'  
    exit 1
fi

obs=WS
date=$1
local_path="$2/$date/$obs"

YYYY=${date:0:4}
MM=${date:4:2}
DD=${date:6:2}

mkdir -p $local_path

remote='ftp://nrt.cmems-du.eu/Core/INSITU_GLO_NRT_OBSERVATIONS_013_030/glo_multiparameter_nrt/latest/'
remote+="$date/"

filelist="*_${obs}_*.nc"
wget -nv -r -N -l1 -nd -np $remote -P $local_path -A $filelist
 
echo -e "\n Done downloading $obs wave data for $date\n"

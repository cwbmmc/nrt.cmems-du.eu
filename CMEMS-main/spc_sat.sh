#!/bin/bash
# download wave spectral information from SAR hosted at CMEMS ftp server

set -u

if [[ $# != 3 ]]; then
    echo 'To use the script: $0 sat date local_path
    sat		: Acronym of one of the available satellites [s1a|s1b] 
    date	: Date in the following format YYYYMMDD 
    local_path	: local/path/to/save/the/data'
    exit 1
fi

sat=$1
date=$2
local_path="$3/$date/$sat"

YYYY=${date:0:4}
MM=${date:4:2}
DD=${date:6:2}

mkdir -p $local_path

remote='ftp://nrt.cmems-du.eu/Core/WAVE_GLO_WAV_L3_SPC_NRT_OBSERVATIONS_014_002/dataset-wav-sar-l3-spc-nrt-global-'
remote+="$sat/$YYYY/$MM/"

echo $remote

filelist="dataset-wav-sar-l3-spc-nrt-global-"$sat"_"$date"T*.nc"

wget -r -N -l1 -nd -np $remote -P $local_path -A $filelist

echo -e "\n Done downloading SPC data from $sat for $date\n"

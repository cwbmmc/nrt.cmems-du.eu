#!/bin/bash
# download significant wave height from altimeters hosted at CMEMS ftp server

set -eu

if [[ $# != 3 ]]; then
    echo 'To use the script: $0 sat date local_path
    sat		: Acronym of one of the available satellites [al|c2|cfo|h2b|j3|s3a|s3b] 
    date	: Date in the following format YYYYMMDD 
    local_path	: local/path/to/save/the/data'  
    exit 1
fi

sat=$1
date=$2
local_path="$3/$date/$sat"

YYYY=${date:0:4}
MM=${date:4:2}
DD=${date:6:2}

mkdir -p $local_path

remote='ftp://nrt.cmems-du.eu/Core/WAVE_GLO_WAV_L3_SWH_NRT_OBSERVATIONS_014_001/dataset-wav-alti-l3-swh-rt-global-'
remote+="$sat/$YYYY/$MM/"

echo $remote

filelist="global_vavh_l3_rt_"$sat"_"$date"T*.nc"

wget -nv -N -nd -np $remote -P $local_path -A $filelist

echo -e "\n Done downloading SWH data from $sat for $date\n"

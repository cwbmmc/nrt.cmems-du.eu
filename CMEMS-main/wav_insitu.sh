#!/bin/bash
# download insitu wave observations hosted at CMEMS ftp server

set -eu

if [[ $# != 2 ]]; then
    echo "To use the script: $0 sat date local_path
    date	: Date in the following format YYYYMMDD 
    local_path	: local/path/to/save/the/data"
    exit 1
fi

obs="TS"
date=$1
local_path="$2/$date/$obs"

YYYY=${date:0:4}
MM=${date:4:2}
DD=${date:6:2}

mkdir -p $local_path

latest_list="index_latest.txt"
filelist="list_$date.txt"

remote='ftp://nrt.cmems-du.eu/Core/INSITU_GLO_NRT_OBSERVATIONS_013_030/glo_multiparameter_nrt/'
wget -nv -r -N -l1 -nd -np $remote -P $local_path -A $latest_list

grep $date "$local_path/$latest_list" | grep 'VGHS\|VHM0\|VAVH\|VH110\|VHZA' | grep -o '\<ftp.*nc\>' > "$local_path/$filelist"

wget -nv -N -nd -np -P $local_path -i "$local_path/$filelist"

rm -rf "$local_path/*.txt"

echo -e "\n Done downloading $obs wave data for $date\n"

This is an easy script to download satellite data from nrt.cmems-du.eu.

You have to prepare `.wgetrc` and put it in the same location of this repository with content:

- `ftp_user=<Your registed ftp account name on nrt.cmems-du.eu>`
- `ftp_password=<Your password of above account>`

And python3 with BeautifulSoup module.

You can change the related location you want to put downloaded data and logs, default will be under the same position of this repository:

- `./data` : to store the downloaded satellite data
- `./out` : normally I will link it to /dev/shm/... , since we just store the link to be downloaded here.
- `./logs` : to store the execution records.

You may also want to modify the variable: `_RemoteLoc` in `ftp_io.sh`, assign it to where you want to store the satellite data finally.

Then rest things you have to do is to make a cronjobs:

> `7 * * * *	${HOME}/nrt.cmems-du.eu/scripts/ftp_io.sh > ${HOME}/nrt.cmems-du.eu/logs/last_results.log`

Something like above.

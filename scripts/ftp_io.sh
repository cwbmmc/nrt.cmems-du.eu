#!/bin/bash
# Add this to cron:
# 7 * * * *	${HOME}/nrt.cmems-du.eu/scripts/ftp_io.sh > ${HOME}/nrt.cmems-du.eu/logs/last_results.log

#_WDir=`dirname $0`/..
_WDir=${HOME}/nrt.cmems-du.eu
WGETRC=${_WDir}/.wgetrc
export WGETRC
_OutDir=/dev/shm/nrt.cmems-du.eu_out
if [ ! -d $_OutDir ]; then mkdir -p $_OutDir ; fi
_DDir=$_WDir/data
if [ ! -d $_DDir ]; then mkdir -p $_DDir ; fi
_CMDir=$_WDir/CMEMS-main
if [ ! -d $_CMDir ]; then echo "*** $_CMDir is not existed, terminating ***"; exit 1 ; fi
_CMScr=$_CMDir/swh_sat.sh
_PyBin=/usr/local/bin/python3
_ScrDir=$_WDir/scripts
_LogDir=$_WDir/logs
if [ ! -d $_LogDir ]; then mkdir -p $_LogDir ; fi
if [ ! -d $_ScrDir ]; then echo "*** Can not find directory for scripts ***"; exit 1 ; fi
_ParPy=$_ScrDir/htmlparse_bs4.py
_Sats="al j3 s3a s3b cfo h2b"
_RemoteLoc="mmcww352@pcc02:~/netcdf/"
if [ -n "$1" ]; then 
	echo "*** Customized date to retrieve = $1 ***"
	_RmtDat=$1
else
	# UTC timezone
	_RmtDat=`date -d '8 hours ago' +%Y%m%d`
fi
#if [ `date +%d` -ne ${_RmtDat:6:2} ]; then _cSavMov=6; else _cSavMov=5; fi
_cSavMov=6

_MAIN_(){
	# Fetch index.html by CMEMS-main scripts
	if [ "`date +%H`" -eq "00" ]; then mkdir -p $_DDir/`date +%Y%m%d`; fi
	for _Sat in $_Sats; do
		if [ ! -d $_DDir/lastest/$_Sat ]; then mkdir -p $_DDir/lastest/$_Sat; fi
		if [ "`date +%H`" -eq "00" -o ! -L $_DDir/`date +%Y%m%d`/$_Sat ]; then 
			#ln -s $_DDir/lastest/$_Sat $_DDir/`date +%Y%m%d`/
			cd $_DDir/`date +%Y%m%d`/
			ln -s ../lastest/$_Sat ./
		fi
		$_CMScr $_Sat $_RmtDat $_DDir

	# Parse the links inside index.html
		$_PyBin $_ParPy $_Sat $_RmtDat

	# Get the latest 5 links inside index.html
	# Last 5 links in case of file is not existed 
	#for _Sat in $_Sats; do
		tail -5 $_OutDir/${_Sat}_filelist.txt > $_OutDir/${_Sat}.link
		if [ -f $_OutDir/${_Sat}.link ]; then rm $_OutDir/${_Sat}_filelist.txt; fi
		cd $_DDir/$_RmtDat/$_Sat/
		wget -c -i $_OutDir/${_Sat}.link &
		if [ "`date +%M`" -lt "12" ]; then sleep 35 ; fi
		if [ "`date +%M`" -gt "11" ]; then wait ; fi
	done

	# Keep clean; not using rm -rf in case of accident
	_Cls="$_OutDir $_DDir $_LogDir"
	_Ct=3
	for _Cl in $_Cls; do
		if [ "$_Cl" == "${_LogDir}" ]; then _Ct=1 ; fi
		find $_Cl/ -type f -ctime +$_Ct -exec rm {} \;
		find $_Cl/ -type l -ctime +1 -exec rm {} \;
		find $_Cl/ -type d -ctime +0 -exec rmdir {} \;
	done
	find $_DDir -name "index.html" -exec rm {} \;

	# Copy files (and dirs) to remote nodes
	for _Sat in $_Sats; do
		_Count=`ls $_DDir/lastest/$_Sat | grep -c 'global'`
		
		if [ "$_Count" -gt "$_cSavMov" ]; then
			for _Obj in $(ls -tr ${_DDir}/lastest/${_Sat}/ | head -`expr $_Count - $_cSavMov`) ; do
				cd $_DDir/lastest/$_Sat/
				if [ ! -d "$_DDir/old/$_Sat/" ]; then mkdir $_DDir/old/$_Sat/ ; fi
				mv $_Obj $_DDir/old/$_Sat/
			done
		fi
	done
	scp -r -p -C $_DDir/$_RmtDat/* $_RemoteLoc
}
_MAIN_

if [ `date +%d` -ne ${_RmtDat:6:2} ]; then
	_RmtDat=`date +%Y%m%d`
	_MAIN_
fi

# Arrange logs
mv $_LogDir/last_results.log $_LogDir/`date +%Y%m%d%H`.log
mv $_LogDir/last_results.err $_LogDir/`date +%Y%m%d%H`.err

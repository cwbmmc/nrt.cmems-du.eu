#!/usr/local/bin/python3

from bs4 import BeautifulSoup
#import datetime
import os
import sys
from pathlib import Path
HOME = str(Path.home())

#_DateVar=datetime.date.today()
#_TDate=str(_DateVar.year)+str(_DateVar.month)+str(_DateVar.day)
_TDate=sys.argv[2]
# print(_TDate)

#_WDir=os.getcwd()+"/.."
_WDir=HOME+"/nrt.cmems-du.eu"
_ODir=_WDir+"/out"
_DataIndexPath=_WDir+"/data/"+_TDate

#_Sats = ["al","s3a","s3b","j3","cfo","h2b"]
#for _Sat in _Sats:

_Sat=sys.argv[1]
print(_Sat)

_DataIndex=_DataIndexPath+"/"+_Sat+"/index.html"
#print (_DataIndex)

with open(_DataIndex, 'r') as _Index:
    contents = _Index.read()
    #soup = BeautifulSoup(contents, 'lxml')
    #soup = BeautifulSoup(html_doc, 'html.parser')
    soup = BeautifulSoup(contents, 'html.parser')

    _fl = open(_ODir+"/"+_Sat+"_filelist.txt","w+")
    for link in soup.find_all('a'):
        #print(link.get('href'))
        _fl.write(link.get('href')+"\n")

    _fl.close()
_Index.close()

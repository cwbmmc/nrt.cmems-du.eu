#!/usr/local/bin/python3

import requests
from pyquery import PyQuery as pq

# getting data
r = requests.get("../data/20201113/j3/index.html")
html_str = r.text
# parsing data
d = pq(html_str)
poster_selector = ".poster img"
poster_src = [i.attr("src") for i in d(poster_selector).items()][0]
print(d(poster_selector))
print(poster_src)
